

//  Created by Alex Tran-Qui on 07/08/2017.
//  Copyright © 2017 Katalysis / Alex Tran Qui (alex@katalysis.io). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

// https://github.com/ethereum/wiki/wiki/RLP
// https://github.com/ethereumjs/rlp/blob/master/index.js

import Foundation


public enum RLPEncodableError: Error, CustomStringConvertible {
    case incompatibleType(elementType: Any)
    case negativeNumber(element: Any)
    
    public var description: String {
        switch self {
        case let .incompatibleType(elementType: elementType):
            let t = type(of: elementType)
            return "RLPEncodableError: Incompatible type \(t)"
    
        case let .negativeNumber(element: element):
        return "RLPEncodableError: Negative number \(element)"
        }
    }
}


protocol UInts {}
extension UInt : UInts {}
extension UInt8 : UInts {}
extension UInt16 : UInts {}
extension UInt32 : UInts {}
extension UInt64 : UInts {}

protocol Ints {}
extension Int : Ints {}
extension Int8 : Ints {}
extension Int16 : Ints {}
extension Int32 : Ints {}
extension Int64 : Ints {}


public struct RLPEncoder {
    public static func encode(_ data: Any?) throws -> [UInt8] {
        
        guard let data = data else { return [0x80] } // nil ~ empty byte array ~ empty string
        var res: [UInt8] = []
        
        switch (data) {
        case let data as [UInt8]:
            if (data == []) { return [0xC0] } // empty array
            res = data
            if (res.count != 1 || res[0] > 127) {
                while(res[0] == 0) {
                    res.remove(at: 0)
                }
                res = RLPEncoder.encodeLength(UInt(res.count), 128) + res
            }
        case let data as Array<Any>:
            for a in data {
                res += try RLPEncoder.encode(a)
            }
            res = RLPEncoder.encodeLength(UInt(res.count), 192) + res
    
        case let data as String:
            res = [UInt8](data.utf8)
            if (res.count != 1 || res[0] > 127) {
                res = RLPEncoder.encodeLength(UInt(res.count), 128) + res
            }
        case let data as UInt8:
            res = encodeUInt(UInt64(data))
        case let data as UInt16:
            res = encodeUInt(UInt64(data))
        case let data as UInt32:
            res = encodeUInt(UInt64(data))
        case let data as UInt64:
            res = encodeUInt(UInt64(data))
        case let data as UInt:
            res = encodeUInt(UInt64(data))
        case let data as Int8:
            if (data < 0) {throw RLPEncodableError.negativeNumber(element: data)}
            res = encodeUInt(UInt64(data))
        case let data as Int16:
            if (data < 0) {throw RLPEncodableError.negativeNumber(element: data)}
            res = encodeUInt(UInt64(data))
        case let data as Int32:
            if (data < 0) {throw RLPEncodableError.negativeNumber(element: data)}
            res = encodeUInt(UInt64(data))
        case let data as Int64:
            if (data < 0) {throw RLPEncodableError.negativeNumber(element: data)}
            res = encodeUInt(UInt64(data))
        case let data as Int:
            if (data < 0) {throw RLPEncodableError.negativeNumber(element: data)}
            res = encodeUInt(UInt64(data))
        default:
            throw RLPEncodableError.incompatibleType(elementType: data)
        }
        return res
    }
    
    static func encodeUInt(_ u: UInt64) -> [UInt8] {
        var res: [UInt8] = []
        var q = UInt64(u)
        if (q == 0) { return [0x80] }
        if q < 128 {
            res.append(UInt8(q))
        } else {
            var r: UInt8 = 0
            while (q != 0) {
                r = UInt8(q % 256)
                q = q / 256
                res.insert(r, at: 0)
            }
            res = RLPEncoder.encodeLength(UInt(res.count), 128) + res
        }
        return res
    }
    
    static func encodeLength (_ len: UInt, _ offset: UInt8) -> [UInt8] {
        if (len < 56) {
            return [UInt8(len) + offset]
        } else {
            let hexLength = String(format: "%02X", len)
            var lenA: [UInt8] = []
            var q = len
            var r: UInt8 = 0
            while (q != 0) {
                r = UInt8(q % 256)
                q = q / 256
                lenA.insert(r, at: 0)
            }
            
            return [offset + UInt8(55) + UInt8((hexLength.count + 1) / 2)] + lenA
        }
    }
}
/*
public struct RLPDecoder {
    public static func decode(_ encoded: [UInt8])-> Any? {
        
    }
    
    

}
*/
